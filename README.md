# Workshop – Kubernetes on Single Board Computers

In this workshop we set up a tiny Kubernetes cluster on Single Board Computers
(SBC) using a load balancer on OpenWRT.

## Overview

The cluster consists of three SBC as cluster nodes

- `pi-kube1`
- `pi-kube2`
- `pi-kube3`

which are connected to an [OpenWRT][openwrt] router. The router is configured
to provide access to the cluster via a load balancer.

![Overview](openwrt-load-balancing.png)

The router defines two separate networks:

- the *management network* `192.168.1.0/24`
- the *cluster network* `192.168.229.0/24`

The router is administered on the *management network*. It can be reached on
address `192.168.1.1`.

In the *cluster network* the router has address `192.169.229.1`. The cluster
nodes have the following addresses:

- `pi-kube1` has address `192.168.229.2`,
- `pi-kube2` has address `192.168.229.3`,
- `pi-kube3` has address `192.168.229.4`.

The load balancer makes the cluster nodes accessible to the *management network*
on address `192.168.1.72` in a [Round-robin (RR)][rr] fashion.

## Basic Router Setup

The router is pre-installed with OpenWRT in default configuration with a
standard password set for `root`.

The first step is to connect your computer to the router using an ethernet
cable. Plug the cable in one of the ports labeled *LAN*. The web interface
should now be accessible on <http://192.168.1.1>. For troubleshooting the `ip`
command can help:

```sh
ip addr
```

Then log in with username `root` and the standard password. Now open
*System* → *Administration*.

![Menu: System → Administration](admin1.png)

On the tab *SSH Access* set

- *Interface* to `lan`,
- un-check *Password authentication*
- un-check *Allow root logins with password*

![SSH Access](admin3.png)

On the tab *SSH-Keys* add your SSH public key. You can generate a new key with

```sh
ssh-keygen -t ed25519
```

![SSH-Keys](admin4.png)

Now access via SSH should be possible:

```sh
ssh -l root 192.168.1.1
```

```
BusyBox v1.35.0 (2023-04-27 20:28:15 UTC) built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt 22.03.5, r20134-5f15225c1e
 -----------------------------------------------------
```

## Internet Connectivity

There are two options:

1. Connect an ethernet cable in the port labeled *WAN* to a network which has
   Internet connection and DHCP. Then no configuration is needed.
2. Use Wi-Fi capabilities of the router to connect to a WLAN. The necessary
   configuration steps are explained below.

### Internet Over WiFi

In the menu select *Network* → *Wireless*. All wireless adaptors of the system
are listed alongside all configured Wi-Fi connections. For every adaptor there
is a connection in access point mode pre-configured with SSID `OpenWRT`, but not
active. Delete or ignore those connections!

![Scan Wifi networks](wifi1.png)

Search for the right network by choosing one adaptor and using the *Scan* button
to search for the Wi-Fi SSID. If the networks SSID appears, hit the button
*Join Network*.

![Enter WPA passphrase](wifi2.png)

Enter the *WPA Passphrase* and leave the *Name of the new network* as is,
i.e. `wwan`.

Now the full configuration dialog for the connection will appear. On the tab
*Advanced Settings* set the *Country Code*. Then *Save* the Dialog and push
*Save & Apply*.

Under *Network* → *Interfaces* there is now an interface `wwan` configured as
*DHCP Client*. This interface should already have an IPv4 address. To add IPv6
connectivity, another interface has to be created. Push the button *Add new
interface*.

![Add IPv6](wifi3.png)

Choose a name e.g. `wwan6`, set *Protocol* to *DHCPv6 client* and select the
Wi-Fi connection as *Device*. After creating the interface the settings dialog
will appear. On the tab *Firewall Settings* and select `wan` for *Create /
Assign firewall-zone*.

![firewall-zone](wifi4.png)

The interface configuration is stored in `/etc/config/network` and should look
like:

```
config interface 'wwan'
	option proto 'dhcp'

config interface 'wwan6'
	option proto 'dhcpv6'
	option reqaddress 'try'
	option reqprefix 'auto'
```

The firewall configuration is stored in `/etc/config/firewall` and will look
like:

```
config zone
	option name 'wan'
	option input 'REJECT'
	option output 'ACCEPT'
	option forward 'REJECT'
	option masq '1'
	option mtu_fix '1'
	list network 'wan'
	list network 'wan6'
	list network 'wwan'
	list network 'wwan6'
```

## General Debugging Advises

- The system log per default is stored only in RAM on OpenWRT and can be
  displayed using `logread`. Constantly following the messages has proven
  invaluable when debugging:

  ```sh
  logread -f
  ```

- Changing the network can easily create situations, where some nodes
  aren't reachable via network anymore. Setting an unusually low DHCP lease time
  can help in such situations, since the nodes refresh their IP configuration
  more often.

- When manually changing configuration files under `/etc/config`, the
  configuration has to be reloaded using `reload_config`. However, most
  configuration changes require a certain service being restarted. In doubt
  reboot the router (command `reboot`). See also Unified Configuration
  Interface (UCI) [documentation][uci].

## Cluster Network Setup

Assuming your computer is connected to the router on ethernet port *LAN1*, the
ports *LAN2*, *LAN3* and *LAN4* are to be connected to the SBC. Those ports
need to be configured to span a new network (`192.168.229.0/24`). The ports are,
however, per default connected to the *bridge* interface `br-lan`. We remove
them from that bridge and add them to a new bridge for the cluster network

### Bridge Device Setup

The first step is to remove those ports (`lan2`, `lan3` and `lan4`) from the
*bridge* interface `br-lan`. Open the in the menu *Network* → *Interfaces* and
select the tab *Devices*.

![device list](kube-net01.png)

*Configure* the bridge `br-lan` and remove the ports `lan2`, `lan3` and `lan4`
in the list *Bridge ports*. Then create a new bridge via the button *Add device
configuration*.

![add device configuration](kube-net02.png)

- Set the *Device Type* to *Bridge device*.
- For the *Device name* enter `br-kube`.
- In the *Brigde port* list select `lan2`, `lan3` and `lan4`.

Then *Save* the Dialog. The configuration file `/etc/config/network` should now
contain the following entry:

```
config device
	option type 'bridge'
	option name 'br-kube'
	list ports 'lan2'
	list ports 'lan3'
	list ports 'lan4'
```

### Interface Setup

In the menu *Network* → *Interfaces* select the tab *Interfaces*. Now click on
*Add new interface*.

![add interface](kube-net03.png)

- Name the interface `kube`,
- set the *Protocol* to *static* and
- select `br-kube` for the *Device*.

Then push *Create interface*.

![configure interface](kube-net04.png)

Now the configuration dialog will open. Set

- the *IPv4 address* to `192.168.229.1` and
- the *IPv4 netmask* to `255.255.255.0`.

Then select the tab *Firewall Settings*.

![create firewall zone](kube-net05.png)

In the field *Create / Assign firewall-zone* enter a new name `kube` in the
empty text input line at the bottom of the list. Then select the tab *DHCP
Server*.

![interface DHCP server](kube-net06.png)

Click on the button *Set up DHCP Server* and then *Save*.

The configuration file `/etc/config/network` should now contain the following
entry:

```
config interface 'kube'
	option proto 'static'
	option device 'br-kube'
	option ipaddr '192.168.229.1'
	option netmask '255.255.255.0'
```

The new interface should now appear in the interface overview.

![interface overview](kube-net07.png)

The cluster network is now configured. At this time we can connect a SBC to one
of the ports and power it on. After a few seconds it will receive an IPv4
address via DHCP.

![DHCP lease for a SBC](kube-net08.png)

### Firewall Zone Setup

However, the default firewall settings prevent the access from your computer to
the SBC and from the SBC to the internet. Therefore, we adjust the settings in
the menu *Network* → *Firewall*. Under *Zones* press the button *Edit* for the
zone *kube*.

![firewall setting for kube zone](kube-net09.png)

In the Settings for the zone *kube*

- add the check to *Masquerading*,
- in *Allow forward to destination zones* add `lan` and `wan` and
- in *Allow forward from source zones* add `lan`.

The *Zones* overview should now look like this:

![zone overview](kube-net10.png)

The configuration file `/etc/config/firewall` should now contain the following
entries:

```
config forwarding
	option src 'kube'
	option dest 'lan'

config forwarding
	option src 'lan'
	option dest 'kube'

config forwarding
	option src 'kube'
	option dest 'wan'
```

### Testing the Connection

Look up the IPv4 address of the SBC (in this case `192.168.229.210`) and try
the `ping` command:

```sh
ping -c 5 192.168.229.210
```

Expected output:

```
PING 192.168.229.210 (192.168.229.210) 56(84) bytes of data.
64 bytes from 192.168.229.210: icmp_seq=1 ttl=63 time=1.38 ms
64 bytes from 192.168.229.210: icmp_seq=2 ttl=63 time=1.26 ms
64 bytes from 192.168.229.210: icmp_seq=3 ttl=63 time=1.39 ms
64 bytes from 192.168.229.210: icmp_seq=4 ttl=63 time=1.40 ms
64 bytes from 192.168.229.210: icmp_seq=5 ttl=63 time=1.22 ms

--- 192.168.229.210 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 1.221/1.333/1.404/0.075 ms
```

Depending on the configuration of your computer you might have to add a route to
your system to be able to access the SBC:

```sh
ip route add 192.168.229.0/24 via 192.168.1.1
```

Alternatively disable Wi-Fi (or any other connections) and use the router as
default gateway.

## Basic Node Setup

Now we connect one node at a time to the cluster network and set up the node. If
you connect all SBC at once they are easy to mix up, since they have all the
same hostname configured per default and will receive a random IPv4 address out
of the DHCP address pool. Therefore, start with one node and when you have that
node configured start again at the beginning of this section with the next node.

### Static DHCP Lease and SSH Access

The first step is to register a static IPv4 address for the node. In the menu
open *Network* → *DHCP and DNS* and select the tab *Static Leases*. The SBC
should appear in the list *Active DHCP Leases*. Copy the *MAC address* of the
SBC. Now push the button *Add* above. Enter for

- *Hostname* `pi-kube1`
- *MAC address* the address you copied before
- *IPv4 address* `192.168.229.2`

(For the next node(s) adjust the hostname and IPv4 address accordingly.)

![static DHCP lease](node1.png)

After pushing button *Save* on the dialog push *Save & Apply* on the *DHCP and
DNS* page.

The node will be reachable on the newly set IPv4 address as soon as the DHCP
lease is renewed. Watch the *Active DHCP Leases*

- either under *Status* → *Overview*
- or under *Network* → *DHCP and DNS*.

The node should now be accessible via SSH:

```sh
ssh -l root 192.168.229.2
```

Now enter the default password. For easier access copy your SSH public key to
the node. This can be achieved using the command `ssh-copy-id`:

```sh
ssh-copy-id root@192.168.229.2
```

Add the following lines to the file `~/.ssh/config`:

```
Host pi-kube1
  Hostname 192.168.229.2
  User root
```

Then the node can be accessed via SSH with the command:

```sh
ssh pi-kube1
```

### System Configuration

Connect to the node via SSH and execute all commands in this section on the
node.

The node should have access to the internet. Verify that using the `ping` command:

```sh
ping -c 5 uhurutec.com
```

If there are connection issues check the steps in sections

- [Cluster Network Setup](#cluster-network-setup) and
- [Static DHCP Lease and SSH Access](#static-dhcp-lease-and-ssh-access).

Now set the hostname:

```sh
hostnamectl hostname pi-kube1
```


## Keepalived Setup

[Keepalived][keepalived] is a external load balancer which distributes incoming
network traffic via [NAT][nat] to multiple hosts. We install it directly on the
router. Connect to the router via SSH. All commands in this section are executed
on the router.

For running *keepalived* the following packages have to be installed:

* `keepalived`
* `ipvsadm`
* `kmod-nf-ipvs`

They can be installed with:

```sh
opkg update
opkg install keepalived ipvsadm kmod-nf-ipvs
```

*Keepalived* needs the kernel module `ip_vs`. The version of *keepalived* which
is available on OpenWRT 22.03.05, is not able to automatically load the module.
Therefore, the module has to be loaded explicitly:

```sh
modprobe ip_vs
```

For the module being loaded automatically on reboot, create the file
`/etc/modules.d/keepalived` with following content:

```
ip_vs
```

The configuration file of *keepalived* is `/etc/keepalived/keepalived.conf`.
Initially the file `/etc/config/keepalived` has to be adjusted to the following
content, to have *keepalived* actually evaluate its configuration file:

```
config globals 'globals'
   option alt_config_file          "/etc/keepalived/keepalived.conf"
```

Then run `reload_config` to apply the changes.

Replace the content of `/etc/keepalived/keepalived.conf` with the following
(creating the file on your computer and transferring it with `scp` is an
option).

```
! Configuration File for keepalived

global_defs {
   router_id LVS_DEVEL
   vrrp_skip_check_adv_addr
   vrrp_strict
   vrrp_garp_interval 5
   vrrp_gna_interval 5
}

vrrp_instance VI_1 {
    state MASTER
    interface br-lan
    virtual_router_id 51
    priority 255
    advert_int 1
    virtual_ipaddress {
        192.168.1.72
    }
}

virtual_server 192.168.1.72 6443 {
    lb_algo rr
    lb_kind NAT
    persistence_timeout 50
    protocol TCP

    real_server 192.168.229.2 6443 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
    real_server 192.168.229.3 6443 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
    real_server 192.168.229.4 6443 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
}

virtual_server 192.168.1.72 80 {
    lb_algo rr
    lb_kind NAT
    persistence_timeout 50
    protocol TCP

    real_server 192.168.229.2 80 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
    real_server 192.168.229.3 80 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
    real_server 192.168.229.4 80 {
        TCP_CHECK {
            connect_timeout 4
        }
    }
}
```

Using this configuration the network traffic which arrives at `192.168.1.72` on
TCP ports 80 and 6443 is distributed in [round-robin][rr] fashion to the hosts
`192.168.229.2`, `192.168.229.3` and `192.168.229.4`.

After changing the configuration the service has to be restarted:

```sh
/etc/init.d/keepalived restart
```

*Keepalived* adds automatically the address `192.168.1.72` to the interface
`br-lan`. A DNS entry can be added under the menu *Network* → *DHCP and DNS* on
tab *Hostnames*. Add an entry with *Hostname* `pi-kube` and *IP address*
`192.168.1.72`. This adds the following entry to `/etc/config/dhcp`:

```
config domain
	option name 'pi-kube'
	option ip '192.168.1.72'
```

## K3s Setup

For the *k3s* setup a common secret token is needed. Such a token can be generated
with:

```sh
pwgen -s 64 1
```

In order to avoid having the token in shell history, the token will be stored in
a shell variable using `read`.

On the first node `pi-kube1` with address `192.168.229.2` *k3s* installation can
be initiated with:

```sh
read -srp "Token: " K3S_TOKEN
export K3S_TOKEN
curl -sfL https://get.k3s.io | sh -s - server \
  --tls-san pi-kube --tls-san 192.168.1.72 \
  --cluster-init
```

On the other nodes:

```sh
read -srp "Token: " K3S_TOKEN
export K3S_TOKEN
curl -sfL https://get.k3s.io | sh -s - server \
--tls-san pi-kube --tls-san 192.168.1.72 \
--server https://192.168.229.2:6443
```

### Accessing the Cluster

If your computer is connected to the internet using the router, it is very
likely that `pi-kube` resolves already to the cluster. Use `getent` to verify:

```sh
getent hosts pi-kube
```

This should yield:

```
192.168.1.72    pi-kube.lan
```

If that is not the case, then add an entry for the cluster to the file
`/etc/hosts` on your computer:

```
192.168.1.72     pi-kube pi-kube.lan
```

The *kubeconfig* for access to the cluster can be extracted from one of the
nodes from the file `/etc/rancher/k3s/k3s.yaml`. Store the content to
`~/.kube/config` on your computer. In the configuration under
`clusters.cluster.server` replace the IP address `127.0.0.1` with the hostname
of the cluster `https://pi-kube:6443`. Additionally, the generic cluster name
`default` should be replaced, e.g. by searching and replacing all occurrences of
`default` with `pi-kube`.

All added nodes are *control plane* nodes. If you wanted to add a worker node to
the cluster, you would run:

```sh
read -srp "Token: " K3S_TOKEN
export K3S_TOKEN
curl -sfL https://get.k3s.io | sh -s - agent \
--server https://192.168.229.2:6443
```

### Traefik Setup

[Traefik][traefik] is the internal load balancer which comes with *k3s*
included. However, the controller is per default deployed only to one host. In
order for the cluster being reachable if one node fails, the number of instances
has to be increased. For that purpose the deployment `traefik` is adjusted:

```sh
kubectl --namespace kube-system edit deploy traefik
```

There `spec.replicas` is set to 2 (or 3).


## Setting up a Service

As example, we deploy a stock [Nginx][nginx]. Therefore, to begin with create
the namespace `nginx`:

```sh
kubectl create namespace nginx
```

The following steps are presented as YAML content. To apply them to the cluster,
save each as file, e.g. `deploy.yaml` and run:

```sh
kubectl apply --filename deploy.yaml
```

Install the deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - image: nginx:latest
          name: nginx
          ports:
            - containerPort: 80
              name: http
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: 80
          readinessProbe:
            httpGet:
              path: /
              port: 80
```

Now create the service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: nginx
spec:
  selector:
    app: nginx
  type: ClusterIP
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
```

Followed by an Ingress rule for traefik:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  namespace: nginx
spec:
  rules:
    - host: "pi-kube"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx
                port:
                  number: 80
```

The service is now accessible via the address of the cluster `http://pi-kube/`.


## License

This workshop [Kubernetes on Single Board Computers][repo] ©2023 by
[UhuruTec AG][uhuru] is licensed under [CC BY-SA 4.0][bysa].

All source code is licensed under [GNU Affero General Public License][agpl].

Embedded code snippets are licensed under both licenses [CC BY-SA 4.0][bysa] and
[GNU Affero General Public License][agpl].

---

[repo]: https://gitlab.com/uhurutec/kubernetes-on-sbc-workshop
[uhuru]: https://uhurutec.com/
[bysa]: LICENSE-BY-SA.md
[agpl]: LICENSE-AGPL3.md
[rr]: https://en.wikipedia.org/wiki/Round-robin_scheduling
[openwrt]: https://openwrt.org/
[uci]: https://openwrt.org/docs/guide-user/base-system/uci
[keepalived]: https://keepalived.readthedocs.io/en/latest/
[nat]: https://en.wikipedia.org/wiki/Network_address_translation
[traefik]: https://doc.traefik.io/traefik/
[nginx]: https://nginx.org/
